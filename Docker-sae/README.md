# TD2-Virtualisation-Harribaud-Henchiri


Afin de pouvoir utiliser les dockers suivant asssurez-vous de bien cloner le [dépot](https://gitlabinfo.iutmontp.univ-montp2.fr/harribaudk/td2-virtualisation-harribaud-henchiri.git).

Une fois dans le dépot, utiliser un terminal et déplacer-vous jusqu'au dossier docker et lancer les services à l'aide des commandes suivante :
```
cd Docker-sae
cd docker2
docker compose up -d
```
Le services sont démarrés vous pouvez le vérifier en tapant dans la barre d'URL :
```
localhost:90
```

Le serveur Apache est donc bien fonctionnel si vous êtes sur le site de notre SAE4 .
